import firebase from 'firebase'

// Initialize Firebase
var config = {
  apiKey: "AIzaSyAKA3dF1NejYMhCt603XFX43O2UVQKYML8",
  authDomain: "hschool-1f924.firebaseapp.com",
  databaseURL: "https://hschool-1f924.firebaseio.com",
  projectId: "hschool-1f924",
  storageBucket: "hschool-1f924.appspot.com",
  messagingSenderId: "298848856834"
}

const appFirebase = firebase.initializeApp(config)

export {appFirebase}
