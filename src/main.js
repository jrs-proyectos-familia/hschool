// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from '@/store/store'

import BootstrapVue from 'bootstrap-vue'

import { sync } from 'vuex-router-sync'
import { appFirebase } from '@/config/firebase'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)

Vue.config.productionTip = false

Vue.prototype.$eventBus = new Vue()

sync(store, router)

appFirebase.auth().onAuthStateChanged(user => {
  /* eslint-disable no-new */
  // Si se coloca fuera de este método, el guard de autentificacion por rutas no funcionará porque no espera a que este listo el servicio de firebase
  new Vue({
    el: '#app',
    store,
    router,
    components: { App },
    template: '<App/>'
  })
})
