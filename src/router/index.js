import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store/store'

import Login from '@/pages/Login/Login'

import School from '@/pages/School/School'
import SchoolCreate from '@/pages/School/SchoolCreate'
import SchoolUpdate from '@/pages/School/SchoolUpdate'

import Auth from '@/pages/Auth/Auth'
import AuthCreate from '@/pages/Auth/AuthCreate'
import AuthVerify from '@/pages/Auth/AuthVerify'

import Profile from '@/pages/Profile/Profile'

import Ranks from '@/pages/Ranks/Ranks'

import Student from '@/pages/Student/Student'

import Classroom from '@/pages/Classroom/Classroom'

import Subject from '@/pages/Subject/Subject'

import Homework from '@/pages/Homework/Homework'

import firebase from 'firebase'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/school',
      name: 'School',
      component: School,
      meta: {
        auth: true
      },
      redirect: {name: 'SchoolCreate'},
      children: [
        {
          path: 'create',
          name: 'SchoolCreate',
          component: SchoolCreate,
          props: {schoolTitle: 'Escuela: Crear'}
        },
        {
          path: 'update',
          name: 'SchoolUpdate',
          component: SchoolUpdate,
          props: true
        },
        {
          path: '*',
          redirect: {name: 'SchoolCreate'}
        }
      ]
    },
    {
      path: '/auth',
      name: 'Auth',
      component: Auth,
      redirect: {name: 'AuthCreate'},
      children: [
        {
          path: 'create',
          name: 'AuthCreate',
          component: AuthCreate,
          props: {authTitle: 'Autentificar: Crear'}
        },
        {
          path: 'verify',
          name: 'AuthVerify',
          component: AuthVerify,
          meta: {
            auth: true
          },
          props: {authTitle: 'Autentificar: Verificar Email'}
        },
        {
          path: '*',
          redirect: {name: 'AuthCreate'}
        }
      ]
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      meta: {
        auth: true
      },
      props: {profileTitle: 'Mi Perfil'}
    },
    {
      path: '/ranks',
      name: 'Ranks',
      component: Ranks,
      meta: {
        auth: true
      },
      props: {ranksTitle: 'Gestionar Rangos'}
    },
    {
      path: '/student',
      name: 'Student',
      component: Student,
      meta: {
        auth: true
      },
      props: {studentTitle: 'Ficha del Estudiante'}
    },
    {
      path: '/classroom',
      name: 'Classroom',
      component: Classroom,
      meta: {
        auth: true
      },
      props: {classroomTitle: 'Gestionar Salones'}
    },
    {
      path: '/subject',
      name: 'Subject',
      component: Subject,
      meta: {
        auth: true
      },
      props: {subjectTitle: 'Gestionar Materias'}
    },
    {
      path: '/homework',
      name: 'Homework',
      component: Homework,
      meta: {
        auth: true
      },
      props: {homeworkTitle: 'Gestionar Tareas'}
    },
    {
      path: '*',
      redirect: {name: 'Login'}
    }
  ]
})

router.beforeEach((to, from, next) => {
  const auth = to.matched.some(param => param.meta.auth)

  if (to.name !== 'AuthVerify') {
    if (auth) {
      const user = firebase.auth().currentUser

      if (user) {
        store.dispatch('saveUser', user)

        if (!store.getters.getUserVerify && store.getters.getUserVerify !== null) {
          next({name: 'AuthVerify'})
        } else {
          firebase.database().ref(`/ranks/${user.uid}`).once('value', data => {
            const collection = data.val()

            if (collection.email === user.email) {
              store.dispatch('saveRank', collection.rank)
              next()
            } else {
              store.dispatch('destroyUser')
              next({name: 'Login'})
            }
          })
        }
      } else {
        store.dispatch('destroyUser')
        next({name: 'Login'})
      }
    } else next()
  } else next()

})


export default router
