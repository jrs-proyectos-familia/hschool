import Vue from 'vue'
import Vuex from 'vuex'

import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  plugins: [
    createPersistedState()
  ],
  state: {
    user: {
      id: null,
      name: null,
      email: null,
      photo: null,
      rank: null,
      verify: null
    }
  },
  getters: {
    getUser (state) {
      if (state.user !== null) {
        return {
          id: state.user.id,
          name: state.user.name ? state.user.name : 'Pendiente',
          email: state.user.email,
          photo: state.user.photo ? state.user.photo : 'Pendiente',
          rank: state.user.rank ? state.user.rank : 'Pendiente'
        }
      }
      return {
        id: null,
        name: null,
        email: null,
        photo: null,
        rank: null
      }
    },

    getUserVerify (state) {
      if (state.user !== null) return state.user.verify
      else return null
    },

    getId (state) {
      if (state.user !== null) return state.user.id
      else return null
    }
  },
  mutations: {
    saveUser (state, user) {
      state.user.id = user.uid
      state.user.name = user.displayName
      state.user.email = user.email
      state.user.photo = user.photoURL
      state.user.verify = user.emailVerified
    },

    saveRank (state, rank) {
      state.user.rank = rank
    },

    destroyUser (state) {
      state.user.id = null
      state.user.name = null
      state.user.email = null
      state.user.photo = null
      state.user.verify = null
    }
  },
  actions: {
    saveUser ({commit}, user) {
      commit('saveUser', user)
    },

    saveRank ({commit}, rank) {
      commit('saveRank', rank)
    },

    destroyUser ({commit}) {
      commit('destroyUser')
    }
  }
})
