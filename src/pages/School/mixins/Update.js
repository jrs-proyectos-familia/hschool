import { ValidatorMethods } from "@/tools/validator";

import { appFirebase } from "@/config/firebase";

const database = appFirebase.database()

export const Update = {
  methods: {
    validatorUpdate(mode, field, type, min, max) {
      const val = ValidatorMethods[mode](
        this.form[field].value,
        type,
        min,
        max
      );

      if (val) {
        this.form[field].error = val;
        this.form[field].state = false;
      } else {
        this.form[field].error = null;
        this.form[field].state = true;
      }
    },

    continueUpdate() {
      let next = true;

      for (const field of Object.keys(this.form)) {
        if (field !== "state" && field !== "picture" && field !== 'institute') {
          if (this.form[field].error !== null) {
            next = false;
            break;
          }
        }
      }

      return next;
    },

    sendUpdate() {
      for (const field of Object.keys(this.form)) {
        if (field !== "state" && field !== "picture" && field !== 'institute') {
          const { validator } = this.form[field];

          if (field === "notes") {
            this.validatorUpdate(
              "basicMainOptionalEmpty",
              field,
              validator.type,
              validator.min,
              validator.max
            );
          } else {
            this.validatorUpdate(
              "basicMain",
              field,
              validator.type,
              validator.min,
              validator.max
            );
          }
        }
      }

      if (this.continueUpdate()) {
        const collection = {
          principal: this.form.principal.value,
          state: this.form.state.value,
          city: this.form.city.value,
          address: this.form.address.value,
          phone: this.form.phone.value,
          email: this.form.email.value,
          web: this.form.web.value,
          notes: this.form.notes.value
        };

        database
          .ref(`/school/${this.collection}`)
          .update(collection)
          .then(() => {
            this.$eventBus.$emit('[MenuForms]:createButton', false)
            this.$router.push({name: 'SchoolCreate'})
          });
      }
    }
  }
}
