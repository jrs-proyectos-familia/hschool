import { ValidatorMethods } from "@/tools/validator";

import { appFirebase } from "@/config/firebase";

const database = appFirebase.database().ref("/school");

export const Create = {
  methods: {
    validatorCreate(mode, field, type, min, max) {
      const val = ValidatorMethods[mode](
        this.form[field].value,
        type,
        min,
        max
      );

      if (val) {
        this.form[field].error = val;
        this.form[field].state = false;
      } else {
        this.form[field].error = null;
        this.form[field].state = true;
      }

      if (field === 'institute') {
        this.$eventBus.$emit('[SchoolCreate]:institute_state', {
          state: this.form[field].state,
          error: val,
        })
      }
    },

    continueCreate() {
      let next = true;

      for (const field of Object.keys(this.form)) {
        if (field !== "state" && field !== "picture") {
          if (this.form[field].error !== null) {
            next = false;
            break;
          }
        }
      }

      return next;
    },

    clearFieldsCreate() {
      for (const field of Object.keys(this.form)) {
        this.form[field].value = "";
        this.form[field].state = null;
        this.form[field].error = null;

        if (field === 'institute') this.$eventBus.$emit('[SchoolCreate]:institute_reset')
      }
    },

    sendCreate() {
      for (const field of Object.keys(this.form)) {
        if (field !== "state" && field !== "picture") {
          const { validator } = this.form[field];

          if (field === "notes") {
            this.validatorCreate(
              "basicMainOptionalEmpty",
              field,
              validator.type,
              validator.min,
              validator.max
            );
          } else {
            this.validatorCreate(
              "basicMain",
              field,
              validator.type,
              validator.min,
              validator.max
            );
          }
        }
      }

      if (this.continueCreate()) {
        const collection = {
          institute: this.form.institute.value,
          principal: this.form.principal.value,
          state: this.form.state.value,
          city: this.form.city.value,
          address: this.form.address.value,
          phone: this.form.phone.value,
          email: this.form.email.value,
          web: this.form.web.value,
          notes: this.form.notes.value
        };

        database
          .push()
          .set(collection)
          .then(() => {
            this.clearFieldsCreate();
          });
      }
    }
  }
}
