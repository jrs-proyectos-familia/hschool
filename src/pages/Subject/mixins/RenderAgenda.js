import { appFirebase } from "@/config/firebase";

export const RenderAgenda = {
  data () {
    return {
      agenda: []
    }
  },

  methods: {
    renderAgenda (collection = null, subject = null) {
      this.agenda = []

      if (!collection) {
        if (subject) {
          appFirebase
            .database()
            .ref(`/subject/${subject}/agenda`)
            .on("value", data => {
              const collection = data.val();

              this.collectionAgenda(collection)
            });
        }
      } else this.collectionAgenda(collection)
    },

    collectionAgenda (collection, array = false, subject = null) {
      if (collection) {
        const collectionSort = Object.keys(collection).sort((a, b) => {
          if (collection[b].order && collection[a].order) {
            const orderA = collection[a].order;
            let orderAVal = null

            if (orderA.toString().indexOf('.') >= 0) orderAVal = parseInt(orderA.split(".").pop());
            else orderAVal = orderA

            const orderB = collection[b].order;
            let orderBVal = null

            if(orderB.toString().indexOf('.') >= 0) orderBVal = parseInt(orderB.split(".").pop());
            else orderBVal = orderB

            return orderAVal - orderBVal;
          }
        });

        const collectionFinal = [];

        for (const subjectSort of collectionSort) {
          const a = this.generateAgenda(
            subjectSort,
            collection[subjectSort]
          );

          collectionFinal.push(a);
        }

        if (array) {
          if (subject) this.agenda.push({subject, collection: collectionFinal})
          else this.agenda.push(collectionFinal)
        } else this.agenda = collectionFinal;
      }
    },

    generateAgenda(key, collection, subject = {}) {
      const collectionFilter = Object.keys(collection).filter(key => {
        return key !== "title" && key !== "order" && key !== 'homework';
      });

      subject["title"] = key;
      subject["order"] = collection.order;
      subject['homework'] = collection.homework ? collection.homework : null;

      if (collectionFilter.length) {
        subject["child"] = [];

        const collectionSort = collectionFilter.sort((a, b) => {
          if (collection[b].order) {
            const orderA = collection[a].order;
            const orderAVal = parseInt(orderA.split(".").pop());

            const orderB = collection[b].order;
            const orderBVal = parseInt(orderB.split(".").pop());

            return orderAVal - orderBVal;
          }
        });

        for (const agenda of collectionSort) {
          const a = this.generateAgenda(agenda, collection[agenda]);
          subject["child"].push(a);
        }

        return subject;
      } else return subject;
    },
  }
}
