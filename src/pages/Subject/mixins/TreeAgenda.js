export const TreeAgenda = {
  methods: {
    getAgenda(parent, mainAgenda, tree, level) {
      const defaultTree = tree

      if (level < 6) {

        for (const agenda of mainAgenda) {
          if (agenda.title === parent) {
            return tree + agenda.title + '/'
            break
          } else if (agenda.child) {
            const a = this.getAgenda(parent, agenda.child, tree += agenda.title + '/', level + 1)

            if (a !== undefined) return a
            else tree = defaultTree
          }
        }

      } else console.log('No hay más allá de 5')
    }
  }
}
