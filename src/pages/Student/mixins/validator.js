import { ValidatorMethods } from "@/tools/validator";

export default {
  methods: {
    studentProfileValidator(mode, value, type, min, max) {
      let val = ValidatorMethods[mode](value, type, min, max);

      return val
    }
  }
}
