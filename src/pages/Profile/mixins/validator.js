import { ValidatorMethods, Validator } from "@/tools/validator";

export default {
  methods: {
    userProfileValidator(mode, value, type, min, max, repeatPassword = null) {
      let val = ValidatorMethods[mode](value, type, min, max);

      if (repeatPassword !== null) {
        val = Validator.equal(value, {
          data: repeatPassword,
          message: "Contraseña"
        });
      }

      return val
    }
  }
}
