export const TreeHomework = {
  data () {
    return {
      treeHomework: []
    }
  },

  methods: {
    getHomework(mainAgenda, tree, level, arraySubjects = false) {
      const defaultTree = tree

      if (level === 1 && !arraySubjects) this.treeHomework = []

      if (level < 6) {
        for (const agenda of mainAgenda) {
          if (agenda.homework !== null) this.treeHomework.push(tree + agenda.title + '/')

          if (agenda.child) {
            this.getHomework(agenda.child, tree += agenda.title + '/', level + 1, arraySubjects)

            tree = defaultTree
          }
        }
      } else {
        console.log('No hay más allá de 5')
      }
    }
  }
}
