export const qualificationCards = {
  filters: {
    numberTeam(value) {
      const number = value.split('_')

      return `Equipo ${number.pop()}`
    }
  },
}
